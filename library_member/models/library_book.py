from odoo import fields, models
from odoo.exceptions import Warning

class Book(models.Model):
    _inherit = 'library.book'

    is_available = fields.Boolean(string='Is Available?')
    partner_id = fields.Many2one(
        'res.partner',
        string='Vendor'
    )

    def check_isbn(self):
        print("\n")
        print("===extending method===")
        print("condition = 7")
        is_pass = False
        if len(self.isbn) == 7:
            is_pass = True
            return True
        else:
            return super().check_isbn()
        print("\n")

