from odoo import fields, models

class Member(models.Model):
    _name = 'library.member'
    _description = 'Library Member'

    card_number = fields.Char(string='Card Number')
    name = fields.Char()
    member_id = fields.Many2one(
        'res.partner',
        string="Member",
        required=True
    )