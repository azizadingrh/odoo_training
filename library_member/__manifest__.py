{
    'name': 'Library Member',
    'description': 'Pengelolaan Member Perpustakaan',
    'author': 'Brainmatics',
    'depends': ['library_app'],
    'data': ['security/ir.model.access.csv',
        'views/book_view.xml',
        'views/library_menu.xml'
    ],
    'application': False

}