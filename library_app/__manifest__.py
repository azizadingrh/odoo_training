{
    'name': 'Library Management',
    'description': 'Pengelolaan Perpustakaan',
    'author': 'Brainmatics',
    'depends': ['base'],
    'data': ['security/library_security.xml',
        'security/ir.model.access.csv',
        'views/library_menu.xml',
        'views/book_view.xml',
        'reports/library_book_report.xml'
        ],
    'application': True,
}