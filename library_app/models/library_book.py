from odoo import fields, models
from odoo.exceptions import Warning

class Book(models.Model):
    _name = 'library.book'
    _description = 'Pengelolaan Buku'

    name = fields.Char(string='Title', required=True)
    isbn = fields.Char(string='ISBN') #tipe fields odoo hal 159
    active = fields.Boolean(string='Active?', default=True)
    date_published = fields.Date()
    image = fields.Binary('Cover')

    def check_isbn(self):
        print("\n")
        print("===method check isbn===")
        is_pass = False
        len_isbn = len(self.isbn)
        print("condition = 13")
        if len_isbn == 13:
            is_pass = True
        else:
            is_pass = False
        print("\n")
        return is_pass

    def button_check_isbn(self):
        if not self.isbn:
            raise Warning("Please insert ISBN for %s" %self.name)
        if self.isbn and not self.check_isbn():
            raise Warning("%s is an invalid ISBN" %self.isbn)
