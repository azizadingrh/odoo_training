from odoo import fields, models

class Course(models.Model):
    _name = 'education.course'
    _description = 'Pengelolaan Pelatihan'

    name = fields.Char(string='Course Name', required=True)
    price = fields.Float(string='Price')
    course_category = fields.Selection([
        ('development', 'Development'),
        ('networking', 'Networking'),
        ('management', 'Management')
    ], string='Course Category')
    image = fields.Binary()
    active = fields.Boolean(string='Active?', default=True)

    def button_archive(self):
        print("\n")
        print("===Button Archive===")
        trainer_obj = self.env['library.member'].search([])
        
        for trainers in trainer_obj:
            print("Name = ", trainers.name)
            print("Card Number = ", trainers.card_number)
            print("")

        print("\n")