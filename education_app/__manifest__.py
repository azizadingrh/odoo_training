{
    'name': 'Education Management',
    'description': 'Pengelolaan Pelatihan',
    'author': 'Brainmatics',
    'depends': ['base'],
    'data': ['security/education_security.xml',
        'security/ir.model.access.csv',
        'views/education_menu.xml',
        'views/course_view.xml'
    ],
    'application': True,
}