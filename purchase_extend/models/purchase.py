from odoo import fields, models, api

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    vendor_phone = fields.Char(related='partner_id.phone')
    vendor_email = fields.Char(related='partner_id.email')

    def active_user(self):
        return self.env.uid

    pic_id = fields.Many2one('res.users', string='Person in Contact', 
                        default=active_user)
    down_payment = fields.Float(string='Down Payment')
    total = fields.Float(string='Total', readonly=True, compute='total_calculation')

    @api.depends('amount_total')
    def total_calculation(self):
        self.total = self.amount_total + self.down_payment
        
    
    @api.model
    def create(self, vals):
        print("\n")
        print("===Extend Create Method===")

        if not vals.get('partner_ref'):
            vals.update({'partner_ref': 'No Vendor Reference (create)'})

        print("\n")
        return super(PurchaseOrder, self).create(vals)

    def write(self, vals):
        res = super(PurchaseOrder, self).write(vals)
        if not self.partner_ref:
            self.partner_ref = 'No Vendor Reference (write)'
        return res