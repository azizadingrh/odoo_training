{
    'name': 'Education Trainer',
    'description': 'Pengelolaan Trainer',
    'author': 'Brainmatics',
    'depends': ['education_app'],
    'data': ['security/ir.model.access.csv',
        'views/course_view.xml',
        'views/trainer_menu.xml',
        'views/trainer_view.xml',
    ],
    'application': False
}