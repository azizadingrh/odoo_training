from odoo import fields, models

class Trainer(models.Model):
    _name = 'course.trainer'
    _description = 'Pengelolaan Trainer'

    trainer_id = fields.Many2one('res.partner', string='Trainer')
    image = fields.Binary(string='Profile Picture')
    name = fields.Char(string='Trainer Name', related='trainer_id.name')
    course_ids = fields.Many2many('education.course', string='Courses')
